#!/usr/bin/env python3

print("Moj program")
print(5)

outfile = open("../out_001_120.tex", "w")

with open("../001_120.txt", "r") as myfile:
    data = myfile.readlines()

for i in data:
    if (0 == i.find("Zadanie ")):
        i = i.replace("Zadanie ", "\n\\item[")
        i = i.replace(".", "]")

    i = i.replace("A .", "\t\\begin{enumerate}[label=\Alph*.]\n\t\t\\item")
    i = i.replace("B .", "\t\t\\item")
    i = i.replace("C .", "\t\t\\item")
    i = i.replace("D .", "\t\t\\item")
    i = i.replace("Literatura", "\t\\end{enumerate}\nLiteratura")
    i = i.replace(" p o ", " po ")
    i = i.replace("P o ", "Po ")
    i = i.replace(" d o ", " do ")
    i = i.replace("D o ", "Do ")
    i = i.replace(" o d ", " od ")
    i = i.replace("O d ", "Od ")
    i = i.replace(" p o ", " po ")
    outfile.write(i)

outfile.close()

#
# print("\n".join(data))
#
